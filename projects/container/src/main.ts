import { loadRemoteEntry } from '@angular-architects/module-federation';

declare global {
  interface Window { mfeUrl: {mfe1:string}; }
}

const CONFIG_FILE_URL = 'http://localhost:5001/assets/config.json'

const getRemotesConfig = async()=>{
  try {
    const previousResult = await fetch(CONFIG_FILE_URL)
    const config = await previousResult.json()
    window.mfeUrl = {
      mfe1: config.mfe1
    }

    return config
  } catch (error) {
    return Promise.reject(false)
  }
}

const init = async()=>{
  try {
    const config = await getRemotesConfig()
    await Promise.all([
      loadRemoteEntry({
        type: 'module',
        remoteEntry: config.mfe1
      })
    ])
    await import('./bootstrap')
  } catch (error) {
    console.error(error)
  }
}

init()
