import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { loadRemoteModule } from '@angular-architects/module-federation';

const routes: Routes = [
  {
    path: 'mfe1',
    loadChildren: () => loadRemoteModule({
      type: 'module',
      // remplazamos remoteEntry: 'http://localhost:5000/remoteEntry.js' por la variable de config,
      remoteEntry: window.mfeUrl.mfe1,
      exposedModule: './Module'
    }).then(m => m.ModuleOneModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
