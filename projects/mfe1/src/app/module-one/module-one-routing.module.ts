import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentOneComponent } from '../component-one/component-one.component';

export const routes: Routes = [
  {
    path: '',
    component: ComponentOneComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ModuleOneRoutingModule { }
