import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentOneComponent } from '../component-one/component-one.component'
import { routes } from './module-one-routing.module'
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ComponentOneComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    ComponentOneComponent
  ]
})
export class ModuleOneModule { }
