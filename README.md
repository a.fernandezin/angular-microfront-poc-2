# POC prueba carga dinamica de módulos federados.

Partiendo de la POC1 un problema que vemos rapidamente es que hay que declarar en tiempo de construcción los módulos federados que vamos a consumir con las rutas correspondientes donde se encuentra cada uno.

Veamos como solventar este problema:

## external-remotes-plugin

Haremos uso de un plugin de webpack

`npm i -D external-remotes-plugin`

Este plugin nos permite alterar el tiempo de ejecución la ubicación donde se buscarán los módulos federados.

> webpack.config.js

```javascript
// ...
const ExternalTemplateRemotesPlugin = require("external-remotes-plugin");

module.exports = {
  // ...
  plugins: [
    new ModuleFederationPlugin({
        // ...
        remotes: {
            "mfe1": "[window.mfeUrl.mfe1]",
        },
        // ...
    }),
    new ExternalTemplateRemotesPlugin(),
  ],
};

```

Declaramos el uso del plugin y vemos que hemos cambiado el valor de `mfe1` por un valor entre corchetes `window.mfeUrl.mfe1`. Como podemos imaginar, este valor corresponde con una variable global y será sustituido por su valor en el momento de obtener el módulo federado.

En este ejemplo se ha usado esta variable global `window.mfeUrl.mfe1` pero esto es personalizable.

## Archivo de configuración

Simulando lo que podría ser un servicio que provee esta información, encontramos un archivo json con datos de los módulos federados en assets/config.json

```json
{
  "mfe1": "http://localhost:5000/remoteEntry.js"
}
```

Este archivo puede construirse como se quiera, en principio necesitaremos únicamente la ruta donde se encuentra cada módulo federado.

## Obtener la información antes de iniciar la app

En el archivo `main.ts` que ya comentabamos en la POC anterior que ahora llamaba asincronamente a `bootstrap.ts` hemos introducido dos cambios, por una parte la obtención del archivo de configuración para tener las rutas de los módulos federados. Y por otra vamos a cargar todos los manifiestos (normalmente llamados `remoteEntry.js`) de los módulos federados que vayamos a usar, de forma que se gestionen correctamente las depdendencias compartidas entre ellos.

```typescript
import { loadRemoteEntry } from '@angular-architects/module-federation';

declare global {
  interface Window { mfeUrl: any; }
}

const CONFIG_FILE_URL = 'http://localhost:5001/assets/config.json'

/*
  Obtenemos la configuración de los módulos federados y la cargamos en una variable global
  window.mfeUrl.mfe1
*/
const getRemotesConfig = async()=>{
  try {
    const previousResult = await fetch(CONFIG_FILE_URL)
    const config = await previousResult.json()
    window.mfeUrl = {
      mfe1: config.mfe1
    }

    return config
  } catch (error) {
    return Promise.reject(false)
  }
}

const init = async()=>{
  try {
    const config = await getRemotesConfig()
    // aqui cargamos con la funcionalidad loadRemoteEntry todos los manifiestos de los
    // módulos federados que vayamos a usar
    await Promise.all([
      loadRemoteEntry({
        type: 'module',
        remoteEntry: config.mfe1
      })
    ])
    // por ultimo cargamos asincronamente la aplicación
    await import('./bootstrap')
  } catch (error) {
    console.error(error)
  }
}

init()
```

## Como hacerlo funcionar

`npm run run:all`